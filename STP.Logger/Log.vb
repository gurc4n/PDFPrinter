﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Sockets
Imports System.Windows.Forms

Public Class Log

    ' Usage Example
    'private static void CreateLogger()
    '{
    'var appName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
    'Log.Initialize(appName, new List<string> { Log.FileWriter, Log.DBWriter }, @"C:\\X\Logs\");
    '}

    Private Shared s_Syncobject As New Object

    Private Shared s_Log As log4net.ILog

    Private Shared s_LogName As String = "Log"

    Private Shared s_SettingsXMLConfig As String = Nothing

    Private Shared ReadOnly Property Log() As log4net.ILog

        Get

            Return s_Log

        End Get

    End Property

    Public Shared Property LogName() As String
        Get
            Return s_LogName
        End Get
        Set(ByVal value As String)
            s_LogName = value
            'Initialize()
        End Set
    End Property

    'Public Shared Property SettingsXMLConfig() As String
    '    Get
    '        Return s_SettingsXMLConfig
    '    End Get
    '    Set(ByVal value As String)
    '        s_SettingsXMLConfig = value
    '        Initialize()
    '        'log4net.Config.XmlConfigurator.Configure(New System.IO.FileInfo(s_SettingsXMLConfig))
    '    End Set
    'End Property


    Public Shared FileWriter As String = "<appender-ref ref=""RollingLogFileAppender"" />"
    Public Shared EventWriter As String = "<appender-ref ref=""EventLogAppender"" />"
    Public Shared DBWriter As String = "<appender-ref ref=""AdoNetAppender"" />"


    Shared Sub Initialize(AppName As String, LogTypes As List(Of String), Optional ByVal LogPath As String = "C:\X\Logs\", Optional ByVal eventID As Integer = 53, Optional ByVal days2Stay As Integer = 5)

        s_LogName = AppName
        s_Log = log4net.LogManager.GetLogger(AppName)
        Dim logXML As String = My.Resources.XMLConf

        If LogPath <> "C:\X\Logs\" Then
            If Not LogPath.EndsWith("\") Then LogPath = LogPath + "\"

            logXML = logXML.Replace("C:\X\Logs\", LogPath)

        End If

        logXML = logXML.Replace("CHANGELOGNAME", AppName)

        logXML = logXML.Replace("CHANGELOGLEVEL", IIf(String.IsNullOrEmpty(My.Settings.LogLevel),
                                                      "All", My.Settings.LogLevel))


        For Each s In LogTypes
            Dim intStart As Integer = logXML.IndexOf("ADDVALUE")
            logXML = logXML.Insert(intStart, s)
        Next
        logXML = logXML.Replace("ADDVALUE", "")
        Dim g As Guid = Guid.NewGuid()
        File.WriteAllText(Path.Combine(Path.GetTempPath(), g.ToString() + ".xml"), logXML)
        SetEventID(eventID)

        log4net.Config.XmlConfigurator.Configure(New FileInfo(Path.Combine(Path.GetTempPath(), g.ToString() + ".xml")))

        CleanLogsByDirectory(LogPath, days2Stay)

    End Sub

    Shared Sub SetEventID(ByVal EventID As Integer)

        Try

            log4net.ThreadContext.Properties("EventID") = EventID

        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    Shared Sub CleanLogsByDirectory(path As String, days2Stay As Integer)

        Try
            Dim checkDate = Date.Today.AddDays(-days2Stay)
            Dim filePaths = Directory.GetFiles(path)

            For Each fPath As Object In filePaths
                Dim fInfo = New FileInfo(fPath)

                If (fInfo.LastWriteTime < checkDate AndAlso fInfo.Extension <> ".log") Then
                    Try

                        fInfo.Delete()
                    Catch e As Exception
                        'Log.Error(e)
                    End Try
                End If

            Next
        Catch e As Exception
            Log.Error(e)
        End Try

    End Sub

    Public Shared Sub Debug(ByVal message As Object)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Debug(message)

        End SyncLock

    End Sub
    Public Shared Sub Debug(ByVal exception As Exception)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Debug(exception.Message, exception)

        End SyncLock

    End Sub

    Public Shared Sub [Error](ByVal message As Object)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Error(message)

        End SyncLock

    End Sub
    Public Shared Sub [Error](ByVal exception As Exception)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Error(exception.Message, exception)

        End SyncLock

    End Sub

    Public Shared Sub Fatal(ByVal message As Object, Optional isImportant As Boolean = False)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Fatal(message)

        End SyncLock

    End Sub
    Public Shared Sub Fatal(ByVal exception As Exception, Optional isImportant As Boolean = False)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Fatal(exception.Message, exception)


        End SyncLock

    End Sub

    Public Shared Sub Info(ByVal message As Object)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Info(message)

        End SyncLock

    End Sub
    Public Shared Sub Info(ByVal exception As Exception)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Info(exception.Message, exception)

        End SyncLock

    End Sub

    Public Shared Sub Warn(ByVal message As Object)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Warn(message)

        End SyncLock

    End Sub
    Public Shared Sub Warn(ByVal exception As Exception)

        SyncLock s_Syncobject

            If Not Log Is Nothing Then Log.Warn(exception.Message, exception)

        End SyncLock

    End Sub



    Public Shared Function GetMachineInfo() As String
        Dim info = String.Empty

        Dim machineName = Environment.MachineName
        Dim domainName = Environment.UserDomainName
        Dim username = Environment.UserName
        Dim osVersion = Environment.OSVersion
        Dim currentDirectory = Environment.CurrentDirectory
        Dim appName = AppDomain.CurrentDomain.FriendlyName
        Dim localIpInfo = GetLocalIp()


        Dim assembly = Reflection.Assembly.GetExecutingAssembly()
        Dim appVersion = FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion

        info += $"<br><br><br>--------------------------------------------------------------------------<br>"
        info += $"Detailed PC Info generated at {DateTime.Now}<br>"
        info += $"<b>Machine Name:</b> {machineName}<br>"
        info += $"<b>IP Address:</b> {localIpInfo}<br>"
        info += $"<b>Domain Name:</b> {domainName}<br>"
        info += $"<b>Username:</b> {username}<br>"
        info += $"<b>OS Version:</b> {osVersion}<br>"
        info += $"<b>Application:</b> {appName}<br>"
        info += $"<b>Application Version:</b> {appVersion}<br>"
        info += $"<b>Current Directory:</b> {currentDirectory}<br>"
        info += $"<br><br><br>"


        Return info
    End Function


    Public Shared Function GetLocalIp() As IPAddress
        Return (From x As IPAddress In Dns.GetHostAddresses(Dns.GetHostName())
                Where x.AddressFamily = AddressFamily.InterNetwork And x.ToString().StartsWith("10.")
                Select x).DefaultIfEmpty(Nothing).FirstOrDefault()
    End Function

End Class