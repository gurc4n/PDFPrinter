﻿Imports System.Drawing.Printing
Imports Spire.Pdf
Imports STP.PDFWriter

Public Class srvPDFPrinter

    Private m_ThreadForPDF As Threading.Thread = Nothing
    Private m_ThreadForListener As Threading.Thread = Nothing
    Private m_PDFBool As Boolean = True

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        'Logger.Log.LogName = "XPDFPrinter"

        'Logger.Log.SettingsXMLConfig = "C:\X\LogSettings_PDFPrinter.xml"
        Logger.Log.Initialize("XPDFPrinter", New List(Of String) From {Logger.Log.FileWriter}, "D:\X\Logs\")
        Logger.Log.Info("Server Started!")
        ' Add any initialization after the InitializeComponent() call.

    End Sub


    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        'm_ThreadForPDF = New Threading.Thread(AddressOf PDFPrinterSub) With {.IsBackground = True}
        'm_ThreadForPDF.Start()
        m_ThreadForListener = New Threading.Thread(AddressOf FolderListener) With {.IsBackground = True}
        m_ThreadForListener.Start()

    End Sub

    Private m_Syncer As New Object

    Private Sub FolderListener()
        While m_PDFBool
            Try

                Dim fileInFolderList As String() = System.IO.Directory.GetFiles(My.Settings.PDFListen)


                For i = fileInFolderList.Count - 1 To 0 Step -1

                    Dim s As String = fileInFolderList(i)
                    Dim newFileName As String = s.Split("\").Last() & Guid.NewGuid().ToString() & ".pdf"
                    System.IO.File.Move(s, My.Settings.PDFSplit & newFileName)
                    Dim newFilePath As String = My.Settings.PDFSplit & newFileName

                    Dim pSharp As PdfSharp.Pdf.PdfDocument = PdfSharp.Pdf.IO.PdfReader.Open(newFilePath, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import)


                    If pSharp.PageCount > 1 Then
                        Logger.Log.Info("Multi Page PDF detected!")
                        Logger.Log.Info("PDF Page Count = " & pSharp.PageCount.ToString())
                        For j = pSharp.PageCount - 1 To 0 Step -1

                            Dim outputDoc As New PdfSharp.Pdf.PdfDocument()
                            outputDoc.Version = pSharp.Version
                            outputDoc.Info.Creator = pSharp.Info.Creator
                            outputDoc.AddPage(pSharp.Pages(j))
                            outputDoc.Save(My.Settings.PDFListen & newFileName + j.ToString + ".pdf")
                            outputDoc.Dispose()

                        Next
                        Try
                            pSharp.Close()
                            pSharp.Dispose()
                        Catch ex As Exception

                        End Try
                        System.IO.File.Delete(newFilePath)
                    Else
                        Dim printerFound As Boolean = False
                        For Each ps As String In PrinterSettings.InstalledPrinters
                            Logger.Log.Info(ps)
                            Dim psCon As String() = ps.Split("\")
                            If newFilePath.Contains(psCon.Last) Then
                                printerFound = True
                                Logger.Log.Info(newFilePath & " - " & ps)
                                Dim pdf As New PdfDocument
                                With pdf
                                    .LoadFromFile(newFilePath)
                                    .PrintDocument.PrinterSettings.PrinterName = ps
                                    .PageScaling = PdfPrintPageScaling.ShrinkOversized
                                    .PageSettings.Orientation = PdfPageOrientation.Landscape
                                    .PrinterName = ps
                                    .PrintDocument.PrinterSettings.Copies = 1
                                    .PrintDocument.Print()
                                    Logger.Log.Info("File Printed!")
                                    Try
                                        .Close()
                                        .Dispose()
                                    Catch ex As Exception

                                    End Try


                                End With
                            End If
                        Next
                        If Not printerFound Then
                            Logger.Log.Info("Printer not found!")
                        End If
                        System.IO.File.Delete(newFilePath)


                    End If


                    Logger.Log.Info("File : " & newFilePath)


                    'End If




                Next



            Catch ex As Exception
                Logger.Log.Error(ex)
            Finally
                Threading.Thread.Sleep(100)
            End Try
        End While
    End Sub

    Private Sub ListProcessor(m_ListPDF As List(Of PDFHolder))
        Try
            Dim ph As PDFHolder = Nothing
            For i = m_ListPDF.Count - 1 To 0 Step -1

                ph = m_ListPDF(i)

                If Not ph.Printed Then

                    If 2 > 1 Then

                        If Not ph.Splitted Then
                            Dim pSharp As PdfSharp.Pdf.PdfDocument = PdfSharp.Pdf.IO.PdfReader.Open(ph.FileName, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import)

                            For j = pSharp.PageCount - 1 To 0 Step -1

                                Dim outputDoc As New PdfSharp.Pdf.PdfDocument()

                                outputDoc.Version = pSharp.Version

                                outputDoc.Info.Creator = pSharp.Info.Creator

                                outputDoc.AddPage(pSharp.Pages(j))

                                outputDoc.Save(ph.FileName.Replace(".pdf", j.ToString() + ".pdf"))
                                Logger.Log.Error(ph.FileName.Replace(".pdf", j.ToString() + ".pdf"))

                                outputDoc.Dispose()

                            Next

                            ph.Splitted = True

                            Dim t As New Threading.Thread(Sub()
                                                              Try
                                                                  pSharp.Close()
                                                                  pSharp.Dispose()
                                                              Catch ex As Exception

                                                              End Try

                                                          End Sub) With {.IsBackground = True}
                            t.Start()
                        End If

                    Else
                        PrintSinglePDF(ph)
                    End If
                End If

            Next
        Catch ex As Exception
            Logger.Log.Error(ex)
        End Try

    End Sub

    Private Sub PrintSinglePDF(ph As PDFHolder)
        Try
            Dim s As String = ph.FileName
            For Each ps As String In PrinterSettings.InstalledPrinters
                Logger.Log.Info(ps)
                Dim psCon As String() = ps.Split("\")
                If s.Contains(psCon.Last) Then
                    Logger.Log.Info(s & " - " & ps)
                    Dim pdf As New PdfDocument
                    With pdf
                        .LoadFromFile(s)
                        .PrintDocument.PrinterSettings.PrinterName = ps
                        .PageScaling = PdfPrintPageScaling.ShrinkOversized
                        .PageSettings.Orientation = PdfPageOrientation.Landscape
                        .PrinterName = ps
                        .PrintDocument.PrinterSettings.Copies = 1
                        .PrintDocument.Print()
                        .Dispose()
                        ph.Printed = True


                    End With
                End If
            Next
            If Not ph.Printed Then
                ph.PrinterNotAvailable = True
            End If
        Catch ex As Exception
            Logger.Log.Error(ex)
        End Try
    End Sub

    Private m_ListPDF As New List(Of PDFHolder)

    Private Class PDFHolder

        Private m_PDFComp As PdfSharp.Pdf.PdfDocument
        Public Property PDFComp() As PdfSharp.Pdf.PdfDocument
            Get
                Return m_PDFComp
            End Get
            Set(ByVal value As PdfSharp.Pdf.PdfDocument)
                m_PDFComp = value
            End Set
        End Property


        Private m_FileName As String
        Public Property FileName() As String
            Get
                Return m_FileName
            End Get
            Set(ByVal value As String)
                m_FileName = value
            End Set
        End Property

        Private m_Splitted As Boolean
        Public Property Splitted() As Boolean
            Get
                Return m_Splitted
            End Get
            Set(ByVal value As Boolean)
                m_Splitted = value
            End Set
        End Property

        Private m_Printed As Boolean
        Public Property Printed() As Boolean
            Get
                Return m_Printed
            End Get
            Set(ByVal value As Boolean)
                m_Printed = value
            End Set
        End Property

        Private m_PrinterNotAvailable As Boolean
        Public Property PrinterNotAvailable() As Boolean
            Get
                Return m_PrinterNotAvailable
            End Get
            Set(ByVal value As Boolean)
                m_PrinterNotAvailable = value
            End Set
        End Property

        Private m_MarkasDelete As Boolean
        Public Property MarkasDelete() As Boolean
            Get
                Return m_MarkasDelete
            End Get
            Set(ByVal value As Boolean)
                m_MarkasDelete = value
            End Set
        End Property


    End Class


    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        m_PDFBool = False
        Try
            If Not m_ThreadForListener.Join(1000) Then
                m_ThreadForListener.Abort()
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class

