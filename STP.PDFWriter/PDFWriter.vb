﻿
Imports System.Drawing.Printing
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports STP.PDFWriter.PrintPdfFile

Namespace PrintPdfFile
    Public Class RawPrinterHelper
        ' Structure and API declarions:
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Private Class DOCINFOA
            <MarshalAs(UnmanagedType.LPStr)>
            Public pDocName As String
            <MarshalAs(UnmanagedType.LPStr)>
            Public pOutputFile As String
            <MarshalAs(UnmanagedType.LPStr)>
            Public pDataType As String
        End Class

#Region "dll Wrappers"
        <DllImport("winspool.Drv", EntryPoint:="OpenPrinterA", SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function OpenPrinter(<MarshalAs(UnmanagedType.LPStr)> szPrinter As String, ByRef hPrinter As IntPtr, pd As IntPtr) As Boolean
        End Function

        <DllImport("winspool.Drv", EntryPoint:="ClosePrinter", SetLastError:=True, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function ClosePrinter(hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.Drv", EntryPoint:="StartDocPrinterA", SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function StartDocPrinter(hPrinter As IntPtr, level As Int32, <[In], MarshalAs(UnmanagedType.LPStruct)> di As DOCINFOA) As Boolean
        End Function

        <DllImport("winspool.Drv", EntryPoint:="EndDocPrinter", SetLastError:=True, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function EndDocPrinter(hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.Drv", EntryPoint:="StartPagePrinter", SetLastError:=True, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function StartPagePrinter(hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.Drv", EntryPoint:="EndPagePrinter", SetLastError:=True, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function EndPagePrinter(hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.Drv", EntryPoint:="WritePrinter", SetLastError:=True, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
        Private Shared Function WritePrinter(hPrinter As IntPtr, pBytes As IntPtr, dwCount As Int32, ByRef dwWritten As Int32) As Boolean
        End Function

        <DllImport("winspool.drv", CharSet:=CharSet.Auto, SetLastError:=True)>
        Public Shared Function GetDefaultPrinter(pszBuffer As StringBuilder, ByRef size As Integer) As Boolean
        End Function

#End Region

#Region "Methods"

        Public Shared Function SendFileToPrinter(pdfFileName As String) As Boolean
            Try
                '#Region "Get Connected Printer Name"
                Dim pd As New PrintDocument()
                Dim dp As New StringBuilder(256)
                Dim size As Integer = dp.Capacity
                If GetDefaultPrinter(dp, size) Then
                    pd.PrinterSettings.PrinterName = dp.ToString().Trim()
                End If
                '#End Region

                ' Open the PDF file.
                Dim fs As New FileStream(pdfFileName, FileMode.Open)
                ' Create a BinaryReader on the file.
                Dim br As New BinaryReader(fs)
                Dim bytes As [Byte]() = New [Byte](fs.Length - 1) {}
                Dim success As Boolean = False
                ' Unmanaged pointer.
                Dim ptrUnmanagedBytes As New IntPtr(0)
                Dim nLength As Integer = Convert.ToInt32(fs.Length)
                ' Read contents of the file into the array.
                bytes = br.ReadBytes(nLength)
                ' Allocate some unmanaged memory for those bytes.
                ptrUnmanagedBytes = Marshal.AllocCoTaskMem(nLength)
                ' Copy the managed byte array into the unmanaged array.
                Marshal.Copy(bytes, 0, ptrUnmanagedBytes, nLength)
                ' Send the unmanaged bytes to the printer.
                success = SendBytesToPrinter(pd.PrinterSettings.PrinterName, ptrUnmanagedBytes, nLength)
                ' Free the unmanaged memory that you allocated earlier.
                Marshal.FreeCoTaskMem(ptrUnmanagedBytes)
                Return success
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Function

        ''' <summary>
        ''' This function gets the printer name and an unmanaged array of bytes, the function sends those bytes to the print queue.
        ''' </summary>
        ''' <param name="szPrinterName">Printer Name</param>
        ''' <param name="pBytes">No. of bytes in the pdf file</param>
        ''' <param name="dwCount">Word count</param>
        ''' <returns>True on success, false on failure</returns>
        Private Shared Function SendBytesToPrinter(szPrinterName As String, pBytes As IntPtr, dwCount As Int32) As Boolean
            Try
                Dim dwError As Int32 = 0, dwWritten As Int32 = 0
                Dim hPrinter As New IntPtr(0)
                Dim di As New DOCINFOA()
                Dim success As Boolean = False
                ' Assume failure unless you specifically succeed.
                di.pDocName = "PDF Document"
                di.pDataType = "RAW"

                ' Open the printer.
                If OpenPrinter(szPrinterName.Normalize(), hPrinter, IntPtr.Zero) Then
                    ' Start a document.
                    If StartDocPrinter(hPrinter, 1, di) Then
                        ' Start a page.
                        If StartPagePrinter(hPrinter) Then
                            ' Write the bytes.
                            success = WritePrinter(hPrinter, pBytes, dwCount, dwWritten)
                            EndPagePrinter(hPrinter)
                        End If
                        EndDocPrinter(hPrinter)
                    End If
                    ClosePrinter(hPrinter)
                End If

                ' If print did not succeed, GetLastError may give more information about the failure.
                If success = False Then
                    dwError = Marshal.GetLastWin32Error()
                End If
                Return success
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Function
#End Region
    End Class
End Namespace

